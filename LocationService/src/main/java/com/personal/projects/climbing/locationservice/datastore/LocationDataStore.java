package com.personal.projects.climbing.locationservice.datastore;

import com.personal.projects.climbing.locationservice.data.ClimbingLocation;
import com.personal.projects.climbing.locationservice.datastore.rowmapper.ClimbingLocationRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.List;
import java.util.Map;

/**
 * Data access class for the 'location' and 'climbing_location' database tables.
 *
 * @author Chris Smith
 */
@Repository
public class LocationDataStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationDataStore.class);

    private SimpleJdbcCall procInsertClimbingLocation;
    private SimpleJdbcCall procDeleteLocationForUser;
    private SimpleJdbcCall procGetOutdoorClimbingLocations;
    private SimpleJdbcCall procGetIndoorClimbingLocations;

    /**
     * Sets the data source and related metadata so the stored procedures can be accessed in the data source
     *
     * @param dataSource a connection to the data source
     */
    @Autowired
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        this.setStoredProcedures(jdbcTemplate);
    }

    /**
     * Inserts the supplied {@link ClimbingLocation} into the database
     *
     * @param climbingLocation the {@link ClimbingLocation} to insert
     * @return the unique identifier of the location in the database
     * @throws DataAccessException on failure to save the location to the database
     */
    public int insertClimbingLocation(ClimbingLocation climbingLocation) throws DataAccessException {
        LOGGER.info("Inserting climbing location {} into database", climbingLocation.getName());

        SqlParameterSource inputParams = new MapSqlParameterSource()
                .addValue("user_id_in", climbingLocation.getUserId())
                .addValue("location_name_in", climbingLocation.getName())
                .addValue("locality_in", climbingLocation.getLocality())
                .addValue("county_in", climbingLocation.getCounty())
                .addValue("country_in", climbingLocation.getCountry())
                .addValue("post_code_in", climbingLocation.getPostCode())
                .addValue("latitude_in", climbingLocation.getLatitude())
                .addValue("longitude_in", climbingLocation.getLongitude())
                .addValue("outdoor_location_in", climbingLocation.isOutdoorLocation());

        try {
            Map<String, Object> result = this.procInsertClimbingLocation.execute(inputParams);
            return (Integer) result.get("location_id_out");
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to insert climbing location {} into database", climbingLocation.getName(), dae);
            throw dae;
        }
    }

    /**
     * Retrieves all outdoor climbing locations from the database that have been saved for the user with the supplied ID
     *
     * @param userId the unique identifier of the user to retrieve the outdoor climbing locations for
     * @return a list of {@link ClimbingLocation}s
     * @throws DataAccessException on failure to retrieve the locations from the database
     */
    public List<ClimbingLocation> getOutdoorClimbingLocations(int userId) throws DataAccessException {
        LOGGER.info("Retrieving outdoor climbing locations for user {} from the database", userId);
        SqlParameterSource inputParams = new MapSqlParameterSource().addValue("user_id_in", userId);

        try {
            Map<String, Object> results = this.procGetOutdoorClimbingLocations.execute(inputParams);
            return (List<ClimbingLocation>) results.get("locations");
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to get outdoor climbing locations for user {} from database", userId, dae);
            throw dae;
        }
    }

    /**
     * Retrieves all indoor climbing locations from the database that have been saved for the user with the supplied ID
     *
     * @param userId the unique identifier of the user to retrieve the indoor climbing locations for
     * @return a list of {@link ClimbingLocation}s
     * @throws DataAccessException on failure to retrieve the locations from the database
     */
    public List<ClimbingLocation> getIndoorClimbingLocations(int userId) throws DataAccessException {
        LOGGER.info("Retrieving indoor climbing locations for user {} from the database", userId);
        SqlParameterSource inputParams = new MapSqlParameterSource().addValue("user_id_in", userId);

        try {
            Map<String, Object> results = this.procGetIndoorClimbingLocations.execute(inputParams);
            return (List<ClimbingLocation>) results.get("locations");
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to get indoor climbing locations for user {} from database", userId, dae);
            throw dae;
        }
    }

    /**
     * Deletes the specified location for the specified user
     *
     * @param userId the unique identifier of the user to delete the location for
     * @return locationId the unique identifier of the location to be deleted
     * @throws DataAccessException on failure to delete the location from the database
     */
    public void deleteLocationForUser(int userId, int locationId) throws DataAccessException {
        LOGGER.info("Deleting location {} from database for user {}", locationId, userId);

        SqlParameterSource inputParams = new MapSqlParameterSource()
                .addValue("user_id_in", userId)
                .addValue("location_id_in", locationId);

        try {
            this.procDeleteLocationForUser.execute(inputParams);
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to delete location {} from database for user {}", locationId, userId, dae);
            throw dae;
        }
    }

    /**
     * Sets the metadata for the stored procedures in this data source
     *
     * @param jdbcTemplate the JDBC template that gives access to the data source
     */
    private void setStoredProcedures(JdbcTemplate jdbcTemplate) {
        this.procInsertClimbingLocation = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_climbing_location_insert")
                .declareParameters(new SqlOutParameter("location_id_out", Types.INTEGER));

        this.procDeleteLocationForUser = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_climbing_location_delete");

        this.procGetOutdoorClimbingLocations = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_climbing_location_get_outdoor_locations_for_user")
                .returningResultSet("locations", new ClimbingLocationRowMapper());

        this.procGetIndoorClimbingLocations = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_climbing_location_get_indoor_locations_for_user")
                .returningResultSet("locations", new ClimbingLocationRowMapper());
    }
}
