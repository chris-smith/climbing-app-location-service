package com.personal.projects.climbing.locationservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores geometry-related information about a place returned from Google Maps' 'Place Search' API.
 *
 * @author Chris Smith
 */
public class Geometry {
    private PlaceCoordinates coordinates;

    /**
     * @return the coordinates of the place that this {@link Geometry} object corresponds to
     */
    @JsonGetter("coordinates")
    public PlaceCoordinates getCoordinates() {
        return this.coordinates;
    }

    /**
     * @param coordinates the coordinates to set
     */
    @JsonSetter("location")
    public void setCoordinates(PlaceCoordinates coordinates) {
        this.coordinates = coordinates;
    }
}
