DELIMITER //

DROP PROCEDURE IF EXISTS `location`.`usp_climbing_location_get_indoor_locations_for_user`//

CREATE PROCEDURE `location`.`usp_climbing_location_get_indoor_locations_for_user`(
	IN user_id_in INT
)
BEGIN

	SELECT	cl.user_id, l.id, cl.outdoor_location, l.location_name, l.locality, l.county, l.country, l.post_code, l.latitude, l.longitude
	FROM	`location`.`climbing_location` cl
	JOIN	`location`.`location` l
	ON		cl.location_id = l.id
	WHERE	cl.user_id = user_id_in AND NOT cl.outdoor_location;

END//

COMMIT//

DELIMITER ;
