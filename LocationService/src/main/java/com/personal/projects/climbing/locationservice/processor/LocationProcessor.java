package com.personal.projects.climbing.locationservice.processor;

import com.personal.projects.climbing.locationservice.data.*;
import com.personal.projects.climbing.locationservice.datastore.LocationDataStore;
import com.personal.projects.climbing.locationservice.enums.LocationType;
import com.personal.projects.climbing.locationservice.exception.PlaceDetailsException;
import com.personal.projects.climbing.locationservice.exception.PlaceSearchException;
import com.personal.projects.climbing.locationservice.rest.GoogleMapsEndpoints;
import com.personal.projects.climbing.locationservice.util.MappingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Processor class for requests related to retrieving and storing location data.
 *
 * @author Chris Smith
 */
@Component
public class LocationProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationProcessor.class);

    @Value("${location.service.nearby-search.max-results-to-return}")
    private long maxNearbyPlaces;

    @Autowired
    private GoogleMapsEndpoints googleMapsEndpoints;

    @Autowired
    private LocationDataStore dataStore;

    /**
     * Processes a request to retrieve basic information about the location with the specified name
     *
     * @param locationName the location to search for
     * @return a {@link Place} object containing details about the location if successfully retrieved from Google
     * Maps; otherwise null
     * @throws PlaceSearchException on failure to retrieve a valid response from Google Maps' 'Place Search' API
     */
    public Place processLocationSearchRequest(String locationName) throws PlaceSearchException {
        PlaceSearchResult placeSearchResult = this.googleMapsEndpoints.searchForLocation(locationName);
        if (placeSearchResult.getStatus().equals(HttpStatus.OK.getReasonPhrase())
                && placeSearchResult.getCandidates().size() > 0) {
            return placeSearchResult.getCandidates().get(0);
        }

        return null;
    }

    /**
     * Retrieves details of the location with the supplied Google Maps API identifier, then saves the details to the
     * database
     *
     * @param userId          the unique identifier for the user that the new climbing location should be saved for
     * @param placeId         the unique identifier of the location/place, as returned from Google Maps' Place Search API
     * @param outdoorLocation a boolean indicating whether this is an outdoor climbing location or not
     * @return the {@link ClimbingLocation} object that was saved to the database if successful; otherwise null
     * @throws PlaceDetailsException on failure to retrieve a valid response from Google Maps' 'Place Details' API
     * @throws DataAccessException   on failure to save the location to the database
     */
    public ClimbingLocation processSaveLocationRequest(int userId, String placeId, boolean outdoorLocation)
            throws PlaceDetailsException, DataAccessException {
        PlaceDetailsResponse placeDetailsResponse = this.googleMapsEndpoints.getLocationDetail(placeId);
        ClimbingLocation climbingLocation = null;
        if (placeDetailsResponse.getStatus().equals(HttpStatus.OK.getReasonPhrase())
                && placeDetailsResponse.getResult() != null) {
            climbingLocation = MappingUtils.placeDetailsToClimbingLocation(
                    userId,
                    outdoorLocation,
                    placeDetailsResponse.getResult());

            int locationId = this.dataStore.insertClimbingLocation(climbingLocation);
            climbingLocation.setLocationId(locationId);
            return climbingLocation;
        }

        return null;
    }

    /**
     * Processes a request to retrieve all the indoor or outdoor climbing locations for the specified user
     *
     * @param userId           the unique identifier of the user to retrieve the climbing locations for
     * @param outdoorLocations a boolean indicating whether the request is to retrieve outdoor or indoor climbing
     *                         locations
     * @return a list of {@link ClimbingLocation}s retrieved from the database
     * @throws DataAccessException on failure to retrieve the locations from the database
     */
    public List<ClimbingLocation> processGetClimbingLocationsRequest(int userId, boolean outdoorLocations)
            throws DataAccessException {
        if (outdoorLocations) {
            return this.dataStore.getOutdoorClimbingLocations(userId);
        }

        return this.dataStore.getIndoorClimbingLocations(userId);
    }

    /**
     * Processes a request to retrieve basic information about locations of the specified type that are nearest to the
     * specified coordinates
     *
     * @param latitude     the latitude of the location to find the nearest places of the specified type for
     * @param longitude    the longitude of the location to find the nearest places of the specified type for
     * @param locationType the {@link LocationType} to search for e.g. climbing centres or outdoor shops
     * @return a list of {@link NearbyPlace} objects containing basic information about each place retrieved from
     * Google Maps
     * @throws PlaceSearchException on failure to retrieve a valid response from Google Maps' (nearby) 'Place Search'
     *                              API
     */
    public List<NearbyPlace> processGetNearestLocationsRequest(String latitude, String longitude, LocationType locationType)
            throws PlaceSearchException {
        NearbySearchResult nearbySearchResult = this.googleMapsEndpoints.findNearestLocations(
                latitude,
                longitude,
                locationType.toString());

        List<NearbyPlace> nearbyPlaces = new ArrayList<NearbyPlace>();
        if (nearbySearchResult.getStatus().equals(HttpStatus.OK.getReasonPhrase())
                && nearbySearchResult.getResults().size() > 0) {
            nearbyPlaces = nearbySearchResult.getResults()
                    .stream()
                    .limit(this.maxNearbyPlaces)
                    .collect(Collectors.toList());
        }

        return nearbyPlaces;
    }
}
