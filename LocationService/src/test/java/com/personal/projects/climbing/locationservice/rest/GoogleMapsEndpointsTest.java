package com.personal.projects.climbing.locationservice.rest;

import com.personal.projects.climbing.locationservice.exception.PlaceDetailsException;
import com.personal.projects.climbing.locationservice.exception.PlaceSearchException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Unit tests for the {@link GoogleMapsEndpoints} class.
 *
 * @author Chris Smith
 */
public class GoogleMapsEndpointsTest {
    private MockWebServer mockWebServer;

    @InjectMocks
    private GoogleMapsEndpoints endpoints;

    /**
     * Starts a {@link MockWebServer} to handle the REST calls made by {@link GoogleMapsEndpoints}
     *
     * @throws IOException
     */
    @BeforeEach
    public void setUp() throws IOException {
        this.mockWebServer = new MockWebServer();
        this.mockWebServer.start();

        MockitoAnnotations.initMocks(this);
        WebClient webClient = WebClient
                .builder()
                .clientConnector(new ReactorClientHttpConnector())
                .baseUrl(this.mockWebServer.url("/").toString())
                .build();

        ReflectionTestUtils.setField(this.endpoints, "webClient", webClient);
        ReflectionTestUtils.setField(
                this.endpoints, "placeSearchUrlTemplate", this.mockWebServer.url("/").toString());
        ReflectionTestUtils.setField(
                this.endpoints, "placeDetailsUrlTemplate", this.mockWebServer.url("/").toString());
        ReflectionTestUtils.setField(
                this.endpoints, "nearbySearchUrlTemplate", this.mockWebServer.url("/").toString());
    }

    /**
     * Shuts down the {@link MockWebServer} after each test
     *
     * @throws IOException
     */
    @AfterEach
    public void tearDown() throws IOException {
        this.mockWebServer.shutdown();
    }

    /**
     * Tests that {@link GoogleMapsEndpoints#searchForLocation(String)} throws a
     * {@link PlaceSearchException} when an error is returned from Google Maps' 'Place Search' API
     */
    @Test
    public void searchForLocation_errorWithRestResponse_throwsPlaceSearchException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        String locationName = "test location";

        // Act / Assert
        assertThrows(PlaceSearchException.class, () -> this.endpoints.searchForLocation(locationName));
    }

    /**
     * Tests that {@link GoogleMapsEndpoints#getLocationDetail(String)} throws a
     * {@link PlaceDetailsException} when an error is returned from Google Maps' 'Place Details' API
     */
    @Test
    public void getLocationDetail_errorWithRestResponse_throwsPlaceDetailsException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        String placeId = "place-id";

        // Act / Assert
        assertThrows(PlaceDetailsException.class, () -> this.endpoints.getLocationDetail(placeId));
    }

    /**
     * Tests that {@link GoogleMapsEndpoints#findNearestLocations(String, String, String)} throws a
     * {@link PlaceSearchException} when an error is returned from Google Maps' (nearby) 'Place Search' API
     */
    @Test
    public void findNearestLocations_errorWithRestResponse_throwsPlaceSearchException() {
        // Arrange
        mockWebServer.enqueue(
                new MockResponse()
                        .setResponseCode(500)
                        .setBody("Error")
                        .setHeader("Content-Type", "application/json"));

        String latitude = "50.111";
        String longitude = "-1.1111";
        String locationType = "climbing centre";

        // Act / Assert
        assertThrows(PlaceSearchException.class, () ->
                this.endpoints.findNearestLocations(latitude, longitude, locationType));
    }
}
