INSERT INTO `location`.`location` (
	`location_name`,
	`locality`,
	`county`,
	`country`,
	`post_code`,
	`latitude`,
	`longitude`)
VALUES
	('The Cuttings, Portland', 'Portland', 'Dorset', 'United Kingdom', 'DT5 1JA', '50.5408', '-2.4237'),
	('Dancing Ledge, Swanage', 'Swanage', 'Dorset', 'United Kingdom', 'BH19 3LD', '50.5972', '-2.0146'),
	('Stanage Edge, Peak District', 'Hope Valley', 'South Yorkshire', 'United Kingdom', 'S32 1DZ', '53.3550', '-1.6461'),
	('Stone Farm, Southern Sandstone', 'East Grinstead', 'West Sussex', 'United Kingdom', 'RH19 4HW', '51.0945', '-0.0303')
ON DUPLICATE KEY UPDATE
	`location_name` = VALUES(`location_name`),
	`locality` = VALUES(`locality`),
	`county` = VALUES(`county`),
	`country` = VALUES(`country`),
	`post_code` = VALUES(`post_code`),
	`latitude` = VALUES(`latitude`),
	`longitude` = VALUES(`longitude`);
	
COMMIT;
