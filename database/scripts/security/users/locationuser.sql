CREATE USER IF NOT EXISTS 'location_user'@'%' IDENTIFIED BY 'LOCATION_USER_PASSWORD_PLACEHOLDER';

GRANT EXECUTE ON location.* TO 'location_user'@'%';
GRANT SELECT ON mysql.proc TO 'location_user'@'%';

COMMIT;
