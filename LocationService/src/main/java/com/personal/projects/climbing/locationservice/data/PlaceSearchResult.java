package com.personal.projects.climbing.locationservice.data;

import java.util.List;

/**
 * Object representation of the JSON returned from Google Maps' 'Place Search' API, after searching for a particular
 * location.
 *
 * @author Chris Smith
 */
public class PlaceSearchResult {
    private String status;
    private List<Place> candidates;

    /**
     * @return the status returned from the API
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the places returned from the API - this should have a max size of 1
     */
    public List<Place> getCandidates() {
        return this.candidates;
    }

    /**
     * @param candidates the candidates to set
     */
    public void setCandidates(List<Place> candidates) {
        this.candidates = candidates;
    }
}
