package com.personal.projects.climbing.locationservice.rest;

import com.personal.projects.climbing.locationservice.data.*;
import com.personal.projects.climbing.locationservice.datastore.LocationDataStore;
import com.personal.projects.climbing.locationservice.enums.LocationType;
import com.personal.projects.climbing.locationservice.exception.PlaceDetailsException;
import com.personal.projects.climbing.locationservice.exception.PlaceSearchException;
import com.personal.projects.climbing.locationservice.processor.LocationProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * A controller that exposes REST APIs for retrieving and storing location data.
 *
 * @author Chris Smith
 */
@RestController
public class LocationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    private LocationProcessor processor;

    @Autowired
    private LocationDataStore locationDataStore;

    /**
     * REST API to search for a location matching the supplied name, via Google Maps' Places Search API
     *
     * @param locationName the name of the location to search for
     * @return a {@link Place} object in JSON format, containing details about the location
     */
    @GetMapping(path = "/location-search", produces = "application/json")
    public ResponseEntity<?> findLocation(@RequestParam(name = "name", required = true) String locationName) {
        LOGGER.info("Received request at /location-search endpoint. Location supplied: {}.", locationName);

        Place searchResult = null;
        try {
            searchResult = this.processor.processLocationSearchRequest(locationName);
        } catch (PlaceSearchException e) {
            LOGGER.error("Error retrieving location data for {} from Google Maps' Place Search API. "
                    + "Response status code: {}.", locationName, e.getStatusCode(), e);
        }

        if (searchResult != null) {
            LOGGER.info("Successfully retrieved location data for {}", locationName);
            return new ResponseEntity<Place>(searchResult, HttpStatus.OK);
        }

        return new ResponseEntity<String>(
                String.format("Error retrieving location data for %s", locationName),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * REST API to save a new climbing location to the database
     *
     * @param request contains details of the new climbing location and the corresponding user
     * @return the {@link ClimbingLocation} that was saved to the database
     */
    @PostMapping(path = "/climbing-locations", consumes = "application/json")
    public ResponseEntity<?> saveClimbingLocation(@RequestBody NewClimbingLocationRequest request) {
        int userId = request.getUserId();
        String placeId = request.getPlaceId();
        boolean outdoorLocation = request.isOutdoorLocation();
        LOGGER.info("Received request at /climbing-locations endpoint to save a climbing location to the database. " +
                        "User ID: {}. Place ID: {}. Outdoor climbing location: {}.",
                userId, placeId, outdoorLocation);

        ClimbingLocation climbingLocation = null;
        try {
            climbingLocation = this.processor.processSaveLocationRequest(userId, placeId, outdoorLocation);
        } catch (PlaceDetailsException e) {
            LOGGER.error("Error retrieving location details for {} from Google Maps' Place Details API. "
                    + "Response status code: {}.", placeId, e.getStatusCode(), e);
        } catch (DataAccessException e) {
            LOGGER.error("Error saving climbing location with place ID {} to the database.", placeId, e);
        }

        if (climbingLocation != null) {
            LOGGER.info("Successfully saved location with place ID {} to the database", placeId);
            return new ResponseEntity<ClimbingLocation>(climbingLocation, HttpStatus.OK);
        }

        return new ResponseEntity<String>(
                String.format("Error saving location with place ID %s to the database", placeId),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * REST API to remove a location from a user's saved locations in the database
     *
     * @param request contains details of a location that is to be removed from a user's saved locations
     * @return a {@link ResponseEntity} indicating whether the request was successfully carried out
     */
    @DeleteMapping(path = "/climbing-locations", consumes = "application/json")
    public ResponseEntity<?> removeLocationForUser(@RequestBody RemoveLocationRequest request) {
        int userId = request.getUserId();
        int locationId = request.getLocationId();
        LOGGER.info("Received request at /climbing-locations endpoint to remove location {} from user {}'s saved " +
                "locations", locationId, userId);

        try {
            this.locationDataStore.deleteLocationForUser(userId, locationId);
        } catch (DataAccessException e) {
            LOGGER.error("Error deleting location {} from database for user {}", userId, locationId, e);
            return new ResponseEntity<String>(
                    String.format("Error removing location %s from user %s's saved locations", locationId, userId),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    /**
     * REST API to retrieve all the outdoor climbing locations saved in the database for the user with the supplied ID
     *
     * @param userId the unique identifier of the user to retrieve the outdoor climbing locations for
     * @return a list of {@link ClimbingLocation}s
     */
    @GetMapping(path = "/climbing-locations/outdoor/{userId}", produces = "application/json")
    public ResponseEntity<?> getOutdoorClimbingLocations(@PathVariable int userId) {
        LOGGER.info("Received request at /climbing-locations/outdoor endpoint to retrieve the outdoor climbing " +
                "locations saved for user with ID {}", userId);

        List<ClimbingLocation> outdoorClimbingLocations = null;
        try {
            outdoorClimbingLocations = this.processor.processGetClimbingLocationsRequest(userId, true);
        } catch (DataAccessException e) {
            LOGGER.error("Error retrieving outdoor climbing locations for user {} from database", userId, e);
        }

        if (outdoorClimbingLocations != null) {
            LOGGER.info("Successfully retrieved {} outdoor climbing locations for user {}",
                    outdoorClimbingLocations.size(), userId);
            return new ResponseEntity<List<ClimbingLocation>>(outdoorClimbingLocations, HttpStatus.OK);
        }

        return new ResponseEntity<String>(
                String.format("Error retrieving outdoor climbing locations for user %s", userId),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * REST API to retrieve all the indoor climbing locations saved in the database for the user with the supplied ID
     *
     * @param userId the unique identifier of the user to retrieve the indoor climbing locations for
     * @return a list of {@link ClimbingLocation}s
     */
    @GetMapping(path = "/climbing-locations/indoor/{userId}", produces = "application/json")
    public ResponseEntity<?> getIndoorClimbingLocations(@PathVariable int userId) {
        LOGGER.info("Received request at /climbing-locations/indoor endpoint to retrieve the indoor climbing " +
                "locations saved for user with ID {}", userId);

        List<ClimbingLocation> indoorClimbingLocations = null;
        try {
            indoorClimbingLocations = this.processor.processGetClimbingLocationsRequest(userId, false);
        } catch (DataAccessException e) {
            LOGGER.error("Error retrieving indoor climbing locations for user {} from database", userId, e);
        }

        if (indoorClimbingLocations != null) {
            LOGGER.info("Successfully retrieved {} indoor climbing locations for user {}",
                    indoorClimbingLocations.size(), userId);
            return new ResponseEntity<List<ClimbingLocation>>(indoorClimbingLocations, HttpStatus.OK);
        }

        return new ResponseEntity<String>(
                String.format("Error retrieving indoor climbing locations for user %s", userId),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * REST API to retrieve the nearest locations of a certain type to the specified coordinates
     *
     * @param locationType the type of location to search for e.g. climbing centres or outdoor shops
     * @param latitude     the latitude of the location to find the nearest places of the specified type for
     * @param longitude    the longitude of the location to find the nearest places of the specified type for
     * @return a list of {@link NearbyPlace}s
     */
    @GetMapping(path = "/nearby-locations/{locationType}", produces = "application/json")
    public ResponseEntity<?> getNearestLocations(
            @PathVariable String locationType,
            @RequestParam(name = "lat", required = true) String latitude,
            @RequestParam(name = "long", required = true) String longitude) {
        LocationType typeOfLocation = locationType.equals(
                LocationType.CLIMBING_CENTRE.toString().replace(" ", "-")) ?
                LocationType.CLIMBING_CENTRE : LocationType.OUTDOOR_SHOP;
        LOGGER.info("Received request at /nearby-locations endpoint to retrieve the {}s nearest to the location with "
                + "latitude {}, longitude {}", typeOfLocation.toString(), latitude, longitude);

        List<NearbyPlace> nearestLocations = null;
        try {
            nearestLocations = this.processor.processGetNearestLocationsRequest(latitude, longitude, typeOfLocation);
        } catch (PlaceSearchException e) {
            LOGGER.error("Error retrieving nearest {}s to lat {}, long {} from Google Maps' (nearby) " +
                            "Place Search API. Response status code: {}.",
                    typeOfLocation.toString(), latitude, longitude, e.getStatusCode(), e);
        }

        if (nearestLocations != null) {
            LOGGER.info("Successfully retrieved {} nearest {}s to location with latitude {}, longitude {}",
                    nearestLocations.size(), typeOfLocation.toString(), latitude, longitude);
            return new ResponseEntity<List<NearbyPlace>>(nearestLocations, HttpStatus.OK);
        }

        return new ResponseEntity<String>(
                String.format("Error retrieving %ss nearest to location with latitude %s, longitude %s",
                        typeOfLocation.toString(), latitude, longitude),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
