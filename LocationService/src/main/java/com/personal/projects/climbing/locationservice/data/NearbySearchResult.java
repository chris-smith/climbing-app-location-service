package com.personal.projects.climbing.locationservice.data;

import java.util.List;

/**
 * Object representation of the JSON returned from Google Maps' (nearby) 'Place Search' API, after searching for
 * places nearby a particular location.
 *
 * @author Chris Smith
 */
public class NearbySearchResult {
    private String status;
    private List<NearbyPlace> results;

    /**
     * @return the status returned from the API
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the nearby places returned from the API
     */
    public List<NearbyPlace> getResults() {
        return this.results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(List<NearbyPlace> results) {
        this.results = results;
    }
}
