package com.personal.projects.climbing.locationservice.rest;

import com.personal.projects.climbing.locationservice.data.*;
import com.personal.projects.climbing.locationservice.datastore.LocationDataStore;
import com.personal.projects.climbing.locationservice.enums.LocationType;
import com.personal.projects.climbing.locationservice.exception.PlaceDetailsException;
import com.personal.projects.climbing.locationservice.exception.PlaceSearchException;
import com.personal.projects.climbing.locationservice.processor.LocationProcessor;
import com.personal.projects.climbing.locationservice.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link LocationController } class.
 *
 * @author Chris Smith
 */
public class LocationControllerTest {
    private Place dummyPlace;
    private ClimbingLocation dummyClimbingLocation;
    private NearbyPlace dummyNearbyPlace;

    @Mock
    private LocationProcessor processor;

    @Mock
    private LocationDataStore locationDataStore;

    @InjectMocks
    private LocationController controller;

    /**
     * Initialises the mocks and creates dummy objects to be used in the tests
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.dummyPlace = TestUtils.createDummyPlace();
        this.dummyClimbingLocation = TestUtils.createDummyClimbingLocation();
        this.dummyNearbyPlace = TestUtils.createDummyNearbyPlace();
    }

    /**
     * Tests that {@link LocationController#findLocation(String)} returns the expected response when a valid location
     * name is supplied
     *
     * @throws PlaceSearchException
     */
    @Test
    public void findLocation_validRequest_returnsExpectedResponse() throws PlaceSearchException {
        // Arrange
        String locationName = "Swanage";
        when(this.processor.processLocationSearchRequest(locationName)).thenReturn(this.dummyPlace);

        // Act
        ResponseEntity<?> response = this.controller.findLocation(locationName);
        Place place = (Place) response.getBody();

        // Assert
        verify(this.processor, times(1)).processLocationSearchRequest(locationName);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.dummyPlace.getPlaceId(), place.getPlaceId());
        assertEquals(this.dummyPlace.getName(), place.getName());
        assertEquals(this.dummyPlace.getFormattedAddress(), place.getFormattedAddress());
        assertEquals(
                this.dummyPlace.getGeometry().getCoordinates().getLatitude(),
                place.getGeometry().getCoordinates().getLatitude());
        assertEquals(
                this.dummyPlace.getGeometry().getCoordinates().getLongitude(),
                place.getGeometry().getCoordinates().getLongitude());
    }

    /**
     * Tests that {@link LocationController#findLocation(String)} returns a {@link HttpStatus#INTERNAL_SERVER_ERROR}
     * response when the required data cannot be retrieved from Google Maps' Place Search API
     *
     * @throws PlaceSearchException
     */
    @Test
    public void findLocation_errorResponseFromGoogleMapsApi_returns500ServerError() throws PlaceSearchException {
        // Arrange
        String locationName = "Swanage";
        String expectedErrorMessage = String.format("Error retrieving location data for %s", locationName);
        when(this.processor.processLocationSearchRequest(locationName)).thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.findLocation(locationName);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link LocationController#saveClimbingLocation(NewClimbingLocationRequest)} returns the expected
     * response when supplied with a valid {@link NewClimbingLocationRequest}
     *
     * @throws PlaceDetailsException
     */
    @Test
    public void saveClimbingLocation_validRequest_returnsExpectedResponse() throws PlaceDetailsException {
        // Arrange
        NewClimbingLocationRequest request = TestUtils.createDummySaveLocationRequest();
        when(this.processor.processSaveLocationRequest(
                request.getUserId(),
                request.getPlaceId(),
                request.isOutdoorLocation()))
                .thenReturn(this.dummyClimbingLocation);

        // Act
        ResponseEntity<?> response = this.controller.saveClimbingLocation(request);
        ClimbingLocation climbingLocation = (ClimbingLocation) response.getBody();

        // Assert
        verify(this.processor, times(1))
                .processSaveLocationRequest(request.getUserId(), request.getPlaceId(), request.isOutdoorLocation());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.dummyClimbingLocation.getUserId(), climbingLocation.getUserId());
        assertEquals(this.dummyClimbingLocation.getLocationId(), climbingLocation.getLocationId());
        assertEquals(this.dummyClimbingLocation.isOutdoorLocation(), climbingLocation.isOutdoorLocation());
        assertEquals(this.dummyClimbingLocation.getName(), climbingLocation.getName());
        assertEquals(this.dummyClimbingLocation.getLocality(), climbingLocation.getLocality());
        assertEquals(this.dummyClimbingLocation.getCounty(), climbingLocation.getCounty());
        assertEquals(this.dummyClimbingLocation.getCountry(), climbingLocation.getCountry());
        assertEquals(this.dummyClimbingLocation.getPostCode(), climbingLocation.getPostCode());
        assertEquals(this.dummyClimbingLocation.getLatitude(), climbingLocation.getLatitude());
        assertEquals(this.dummyClimbingLocation.getLongitude(), climbingLocation.getLongitude());
    }

    /**
     * Tests that {@link LocationController#saveClimbingLocation(NewClimbingLocationRequest)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the required data cannot be retrieved from Google Maps'
     * Place Details API
     *
     * @throws PlaceDetailsException
     */
    @Test
    public void saveClimbingLocation_errorResponseFromGoogleMapsApi_returns500ServerError() throws PlaceDetailsException {
        // Arrange
        NewClimbingLocationRequest request = TestUtils.createDummySaveLocationRequest();
        String expectedErrorMessage = String.format(
                "Error saving location with place ID %s to the database",
                request.getPlaceId());
        when(this.processor.processSaveLocationRequest(
                request.getUserId(),
                request.getPlaceId(),
                request.isOutdoorLocation()))
                .thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.saveClimbingLocation(request);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link LocationController#removeLocationForUser(RemoveLocationRequest)} calls
     * {@link LocationDataStore#deleteLocationForUser(int, int)} and returns a {@link HttpStatus#OK} response when
     * supplied with a valid {@link RemoveLocationRequest}
     */
    @Test
    public void removeLocationForUser_validRequest_callsLocationDataStoreMethod() {
        // Arrange
        RemoveLocationRequest request = TestUtils.createDummyRemoveLocationRequest();

        // Act
        ResponseEntity<?> response = this.controller.removeLocationForUser(request);

        // Assert
        verify(this.locationDataStore, times(1))
                .deleteLocationForUser(request.getUserId(), request.getLocationId());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Tests that {@link LocationController#removeLocationForUser(RemoveLocationRequest)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when there is an error connecting to the database
     */
    @Test
    public void removeLocationForUser_errorConnectingToDatabase_returns500ServerError() {
        // Arrange
        RemoveLocationRequest request = TestUtils.createDummyRemoveLocationRequest();
        String expectedErrorMessage = String.format(
                "Error removing location %s from user %s's saved locations",
                request.getLocationId(),
                request.getUserId());

        doThrow(new DataAccessException("Error connecting to database") {
        }).when(this.locationDataStore).deleteLocationForUser(request.getUserId(), request.getLocationId());

        // Act
        ResponseEntity<?> response = this.controller.removeLocationForUser(request);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link LocationController#getOutdoorClimbingLocations(int)} returns the expected response when a
     * valid user ID is supplied
     */
    @Test
    @SuppressWarnings("unchecked")
    public void getOutdoorClimbingLocations_validRequest_returnsExpectedResponse() {
        // Arrange
        when(this.processor.processGetClimbingLocationsRequest(anyInt(), anyBoolean()))
                .thenReturn(Collections.singletonList(this.dummyClimbingLocation));

        // Act
        ResponseEntity<?> response =
                this.controller.getOutdoorClimbingLocations(this.dummyClimbingLocation.getUserId());
        List<ClimbingLocation> climbingLocations = (List<ClimbingLocation>) response.getBody();

        // Assert
        verify(this.processor, times(1)).processGetClimbingLocationsRequest(
                this.dummyClimbingLocation.getUserId(),
                true);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, climbingLocations.size());
        assertEquals(this.dummyClimbingLocation.getUserId(), climbingLocations.get(0).getUserId());
        assertEquals(this.dummyClimbingLocation.getLocationId(), climbingLocations.get(0).getLocationId());
        assertEquals(this.dummyClimbingLocation.isOutdoorLocation(), climbingLocations.get(0).isOutdoorLocation());
        assertEquals(this.dummyClimbingLocation.getName(), climbingLocations.get(0).getName());
        assertEquals(this.dummyClimbingLocation.getLocality(), climbingLocations.get(0).getLocality());
        assertEquals(this.dummyClimbingLocation.getCounty(), climbingLocations.get(0).getCounty());
        assertEquals(this.dummyClimbingLocation.getCountry(), climbingLocations.get(0).getCountry());
        assertEquals(this.dummyClimbingLocation.getPostCode(), climbingLocations.get(0).getPostCode());
        assertEquals(this.dummyClimbingLocation.getLatitude(), climbingLocations.get(0).getLatitude());
        assertEquals(this.dummyClimbingLocation.getLongitude(), climbingLocations.get(0).getLongitude());
    }

    /**
     * Tests that {@link LocationController#getOutdoorClimbingLocations(int)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the climbing locations can't be retrieved from the
     * database
     */
    @Test
    public void getOutdoorClimbingLocations_errorConnectingToDatabase_returns500ServerError() {
        // Arrange
        int userId = 5;
        String expectedErrorMessage = String.format("Error retrieving outdoor climbing locations for user %s", userId);
        when(this.processor.processGetClimbingLocationsRequest(userId, true)).thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.getOutdoorClimbingLocations(userId);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link LocationController#getIndoorClimbingLocations(int)} returns the expected response when a
     * valid user ID is supplied
     */
    @Test
    @SuppressWarnings("unchecked")
    public void getIndoorClimbingLocations_validRequest_returnsExpectedResponse() {
        // Arrange
        this.dummyClimbingLocation.setOutdoorLocation(false);
        when(this.processor.processGetClimbingLocationsRequest(anyInt(), anyBoolean()))
                .thenReturn(Collections.singletonList(this.dummyClimbingLocation));

        // Act
        ResponseEntity<?> response =
                this.controller.getIndoorClimbingLocations(this.dummyClimbingLocation.getUserId());
        List<ClimbingLocation> climbingLocations = (List<ClimbingLocation>) response.getBody();

        // Assert
        verify(this.processor, times(1)).processGetClimbingLocationsRequest(
                this.dummyClimbingLocation.getUserId(),
                false);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, climbingLocations.size());
        assertEquals(this.dummyClimbingLocation.getUserId(), climbingLocations.get(0).getUserId());
        assertEquals(this.dummyClimbingLocation.getLocationId(), climbingLocations.get(0).getLocationId());
        assertEquals(this.dummyClimbingLocation.isOutdoorLocation(), climbingLocations.get(0).isOutdoorLocation());
        assertEquals(this.dummyClimbingLocation.getName(), climbingLocations.get(0).getName());
        assertEquals(this.dummyClimbingLocation.getLocality(), climbingLocations.get(0).getLocality());
        assertEquals(this.dummyClimbingLocation.getCounty(), climbingLocations.get(0).getCounty());
        assertEquals(this.dummyClimbingLocation.getCountry(), climbingLocations.get(0).getCountry());
        assertEquals(this.dummyClimbingLocation.getPostCode(), climbingLocations.get(0).getPostCode());
        assertEquals(this.dummyClimbingLocation.getLatitude(), climbingLocations.get(0).getLatitude());
        assertEquals(this.dummyClimbingLocation.getLongitude(), climbingLocations.get(0).getLongitude());
    }

    /**
     * Tests that {@link LocationController#getIndoorClimbingLocations(int)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the climbing locations can't be retrieved from the
     * database
     */
    @Test
    public void getIndoorClimbingLocations_errorConnectingToDatabase_returns500ServerError() {
        // Arrange
        int userId = 5;
        String expectedErrorMessage = String.format("Error retrieving indoor climbing locations for user %s", userId);
        when(this.processor.processGetClimbingLocationsRequest(userId, false)).thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.getIndoorClimbingLocations(userId);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link LocationController#getNearestLocations(String, String, String)} returns the expected
     * response when valid request parameters are supplied
     *
     * @throws PlaceSearchException
     */
    @Test
    public void getNearestLocations_validRequest_returnsExpectedResponse() throws PlaceSearchException {
        // Arrange
        String locationType = "climbing-centre";
        String latitude = "50.1111";
        String longitude = "-1.1111";
        when(this.processor.processGetNearestLocationsRequest(latitude, longitude, LocationType.CLIMBING_CENTRE))
                .thenReturn(Collections.singletonList(this.dummyNearbyPlace));

        // Act
        ResponseEntity<?> response = this.controller.getNearestLocations(locationType, latitude, longitude);
        List<NearbyPlace> nearbyPlaces = (List<NearbyPlace>) response.getBody();

        // Assert
        verify(this.processor, times(1))
                .processGetNearestLocationsRequest(latitude, longitude, LocationType.CLIMBING_CENTRE);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, nearbyPlaces.size());
        assertEquals(this.dummyNearbyPlace.getPlaceId(), nearbyPlaces.get(0).getPlaceId());
        assertEquals(this.dummyNearbyPlace.getName(), nearbyPlaces.get(0).getName());
        assertEquals(this.dummyNearbyPlace.getFormattedAddress(), nearbyPlaces.get(0).getFormattedAddress());
        assertEquals(
                this.dummyNearbyPlace.getGeometry().getCoordinates().getLatitude(),
                nearbyPlaces.get(0).getGeometry().getCoordinates().getLatitude());
        assertEquals(
                this.dummyNearbyPlace.getGeometry().getCoordinates().getLongitude(),
                nearbyPlaces.get(0).getGeometry().getCoordinates().getLongitude());
        assertEquals(this.dummyNearbyPlace.getBusinessStatus(), nearbyPlaces.get(0).getBusinessStatus());
        assertEquals(this.dummyNearbyPlace.getUserRatingAvg(), nearbyPlaces.get(0).getUserRatingAvg());
        assertEquals(this.dummyNearbyPlace.getUserRatingsCount(), nearbyPlaces.get(0).getUserRatingsCount());
    }

    /**
     * Tests that {@link LocationController#getNearestLocations(String, String, String)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when the required data cannot be retrieved from Google Maps'
     * (nearby) Place Search API
     *
     * @throws PlaceSearchException
     */
    @Test
    public void getNearestLocations_errorResponseFromGoogleMapsApi_returns500ServerError() throws PlaceSearchException {
        // Arrange
        String locationType = "climbing-centre";
        String latitude = "50.1111";
        String longitude = "-1.1111";
        String expectedErrorMessage = String.format(
                "Error retrieving climbing centres nearest to location with latitude %s, longitude %s",
                latitude,
                longitude);
        when(this.processor.processGetNearestLocationsRequest(latitude, longitude, LocationType.CLIMBING_CENTRE))
                .thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.getNearestLocations(locationType, latitude, longitude);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }
}
