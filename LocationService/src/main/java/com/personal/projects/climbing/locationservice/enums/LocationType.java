package com.personal.projects.climbing.locationservice.enums;

/**
 * An enum that stores the different types of locations that can be used as a parameter when searching for the
 * 'nearest' locations to a specific place, via Google Maps' (nearby) 'Place Search' API.
 *
 * @author Chris Smith
 */
public enum LocationType {
    CLIMBING_CENTRE("climbing centre"),
    OUTDOOR_SHOP("outdoor shop");

    private final String value;

    /**
     * Constructor for creating an instance of {@link LocationType}
     *
     * @param value the value to set
     */
    LocationType(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
