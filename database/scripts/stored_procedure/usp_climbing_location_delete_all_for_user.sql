DELIMITER //

DROP PROCEDURE IF EXISTS `location`.`usp_climbing_location_delete_all_for_user`//

CREATE PROCEDURE `location`.`usp_climbing_location_delete_all_for_user`(
	IN user_id_in INT
)
BEGIN

	DELETE FROM	`location`.`climbing_location`
	WHERE user_id = user_id_in;

END//

COMMIT//

DELIMITER ;
