package com.personal.projects.climbing.locationservice.data;

/**
 * Contains details of a request for a new climbing location to be saved to the database for a particular user.
 *
 * @author Chris Smith
 */
public class NewClimbingLocationRequest {
    private int userId;
    private String placeId;
    private boolean outdoorLocation;

    /**
     * @return the unique identifier for the user that the new climbing location should be saved for
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the unique identifier of the location/place that is to be saved, as returned from Google Maps' Place
     * Search API
     */
    public String getPlaceId() {
        return this.placeId;
    }

    /**
     * @param placeId the placeId to set
     */
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     * @return a boolean indicating whether this is an outdoor climbing location or not
     */
    public boolean isOutdoorLocation() {
        return this.outdoorLocation;
    }

    /**
     * @param outdoorLocation the outdoorLocation to set
     */
    public void setOutdoorLocation(boolean outdoorLocation) {
        this.outdoorLocation = outdoorLocation;
    }
}
