package com.personal.projects.climbing.locationservice.data;

/**
 * Object representation of the JSON returned from Google Maps' 'Place Details' API, after supplying a unique place ID.
 *
 * @author Chris Smith
 */
public class PlaceDetailsResponse {
    private String status;
    private PlaceDetails result;

    /**
     * @return the status returned from the API
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the details of the place, as returned from the API
     */
    public PlaceDetails getResult() {
        return this.result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(PlaceDetails result) {
        this.result = result;
    }
}
