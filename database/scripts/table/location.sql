DELIMITER //

USE `location`//

CREATE TABLE IF NOT EXISTS `location`.`location`
(
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the location',
	`location_name` VARCHAR(100) NOT NULL COMMENT 'The name of the location',
	`locality` VARCHAR(100) NULL COMMENT 'The town, village or city of the location',
	`county` VARCHAR(100) NULL COMMENT 'The county or state of the location',
	`country` VARCHAR(100) NULL COMMENT 'The country of the location',
	`post_code` VARCHAR(20) NULL COMMENT 'The post code of the location',
	`latitude` DECIMAL(10, 8) NULL COMMENT 'The latitude of the location\'s geo location',
	`longitude` DECIMAL(11, 8) NULL COMMENT 'The longitude of the location\'s geo location',
	PRIMARY KEY (id),
	UNIQUE KEY (`latitude`, `longitude`)
) ENGINE=InnoDB COMMENT='Stores location details' CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci//

COMMIT//

DELIMITER ;
