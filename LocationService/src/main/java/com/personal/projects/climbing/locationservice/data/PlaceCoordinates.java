package com.personal.projects.climbing.locationservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores the latitude and longitude of a place returned from Google Maps' 'Place Search' API.
 *
 * @author Chris Smith
 */
public class PlaceCoordinates {
    private double latitude;
    private double longitude;

    /**
     * @return the latitude of the place
     */
    @JsonGetter("latitude")
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    @JsonSetter("lat")
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude of the place
     */
    @JsonGetter("longitude")
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    @JsonSetter("lng")
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
