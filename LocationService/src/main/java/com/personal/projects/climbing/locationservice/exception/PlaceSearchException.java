package com.personal.projects.climbing.locationservice.exception;

import org.springframework.http.HttpStatus;

/**
 * An exception that is thrown when there is an error retrieving details of a location via Google Maps' 'Place
 * Search' API.
 *
 * @author Chris Smith
 */
public class PlaceSearchException extends Exception {
    private HttpStatus statusCode;

    /**
     * Constructor for creating an instance of {@link PlaceSearchException}
     */
    public PlaceSearchException() {

    }

    /**
     * Constructor for creating an instance of {@link PlaceSearchException}
     *
     * @param message    the exception message detail
     * @param statusCode the {@link HttpStatus} of the response that caused this exception
     * @param cause      the cause of the exception
     */
    public PlaceSearchException(String message, HttpStatus statusCode, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * @return the {@link HttpStatus} of the response that caused this exception
     */
    public HttpStatus getStatusCode() {
        return this.statusCode;
    }
}
