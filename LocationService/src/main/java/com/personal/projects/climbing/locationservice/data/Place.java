package com.personal.projects.climbing.locationservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Stores the basic information about a place returned from Google Maps' 'Place Search' API.
 *
 * @author Chris Smith
 */
public class Place {
    private String placeId;
    private String name;
    private String formattedAddress;
    private Geometry geometry;

    /**
     * @return the unique identifier of this place in the Places API
     */
    @JsonGetter("placeId")
    public String getPlaceId() {
        return this.placeId;
    }

    /**
     * @param placeId the placeId to set
     */
    @JsonSetter("place_id")
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     * @return the name of this place
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the user-friendly, formatted address of this place
     */
    @JsonGetter("formattedAddress")
    public String getFormattedAddress() {
        return this.formattedAddress;
    }

    /**
     * @param formattedAddress the formattedAddress to set
     */
    @JsonSetter("formatted_address")
    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return the geometry information relating to this place
     */
    public Geometry getGeometry() {
        return this.geometry;
    }

    /**
     * @param geometry the geometry to set
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }
}
