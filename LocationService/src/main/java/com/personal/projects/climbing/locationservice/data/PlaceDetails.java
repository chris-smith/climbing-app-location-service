package com.personal.projects.climbing.locationservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

/**
 * Stores detailed information about a place returned from Google Maps' 'Place Details' API.
 *
 * @author Chris Smith
 */
public class PlaceDetails {
    private String placeId;
    private String name;
    private String formattedAddress;
    private Geometry geometry;
    private List<AddressComponent> addressComponents;

    /**
     * @return the unique identifier of this place in the Places API
     */
    public String getPlaceId() {
        return this.placeId;
    }

    /**
     * @param placeId the placeId to set
     */
    @JsonSetter("place_id")
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     * @return the name of this place
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the user-friendly, formatted address of this place
     */
    public String getFormattedAddress() {
        return this.formattedAddress;
    }

    /**
     * @param formattedAddress the formattedAddress to set
     */
    @JsonSetter("formatted_address")
    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return the geometry information relating to this place
     */
    public Geometry getGeometry() {
        return this.geometry;
    }

    /**
     * @param geometry the geometry to set
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    /**
     * @return the individual components of this place's address
     */
    public List<AddressComponent> getAddressComponents() {
        return this.addressComponents;
    }

    /**
     * @param addressComponents the addressComponents to set
     */
    @JsonSetter("address_components")
    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }
}
