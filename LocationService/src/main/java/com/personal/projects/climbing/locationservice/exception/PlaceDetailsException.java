package com.personal.projects.climbing.locationservice.exception;

import org.springframework.http.HttpStatus;

/**
 * An exception that is thrown when there is an error retrieving details of a location via Google Maps' 'Place
 * Details' API.
 *
 * @author Chris Smith
 */
public class PlaceDetailsException extends Exception {
    private HttpStatus statusCode;

    /**
     * Constructor for creating an instance of {@link PlaceDetailsException}
     */
    public PlaceDetailsException() {

    }

    /**
     * Constructor for creating an instance of {@link PlaceDetailsException}
     *
     * @param message    the exception message detail
     * @param statusCode the {@link HttpStatus} of the response that caused this exception
     * @param cause      the cause of the exception
     */
    public PlaceDetailsException(String message, HttpStatus statusCode, Throwable cause) {
        super(message, cause);
        this.statusCode = statusCode;
    }

    /**
     * @return the {@link HttpStatus} of the response that caused this exception
     */
    public HttpStatus getStatusCode() {
        return this.statusCode;
    }
}
