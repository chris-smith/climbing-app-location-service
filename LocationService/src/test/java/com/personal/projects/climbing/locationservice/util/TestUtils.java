package com.personal.projects.climbing.locationservice.util;

import com.personal.projects.climbing.locationservice.data.*;
import com.personal.projects.climbing.locationservice.enums.BusinessStatus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility methods to be used throughout the tests.
 *
 * @author Chris Smith
 */
public final class TestUtils {
    /**
     * Default private constructor to prevent instantiation
     */
    private TestUtils() {

    }

    /**
     * Creates a dummy {@link Place} object to be used in the tests
     *
     * @return the dummy {@link Place} object
     */
    public static Place createDummyPlace() {
        Place place = new Place();
        place.setPlaceId("place-id");
        place.setName("Swanage");
        place.setFormattedAddress("Swanage, Dorset, UK");
        PlaceCoordinates coords = new PlaceCoordinates();
        coords.setLatitude(50.1111);
        coords.setLongitude(-1.1111);
        Geometry geometry = new Geometry();
        geometry.setCoordinates(coords);
        place.setGeometry(geometry);
        return place;
    }

    /**
     * Creates a dummy {@link PlaceSearchResult} object to be used in the tests
     *
     * @param places the list of {@link Place}s that should be set as the results returned
     * @param status the response status that should be set on the result
     * @return the dummy {@link PlaceSearchResult}
     */
    public static PlaceSearchResult createDummyPlaceSearchResult(List<Place> places, String status) {
        PlaceSearchResult dummyPlaceSearchResult = new PlaceSearchResult();
        dummyPlaceSearchResult.setStatus(status);
        dummyPlaceSearchResult.setCandidates(places);
        return dummyPlaceSearchResult;
    }

    /**
     * Creates a dummy {@link ClimbingLocation} object to be used in the tests
     *
     * @return the dummy {@link ClimbingLocation} object
     */
    public static ClimbingLocation createDummyClimbingLocation() {
        ClimbingLocation climbingLocation = new ClimbingLocation();
        climbingLocation.setUserId(100);
        climbingLocation.setLocationId(3);
        climbingLocation.setOutdoorLocation(true);
        climbingLocation.setName("The Cuttings, Portland");
        climbingLocation.setLocality("Portland");
        climbingLocation.setCounty("Dorset");
        climbingLocation.setCountry("United Kingdom");
        climbingLocation.setPostCode("PORTLAND");
        climbingLocation.setLatitude(50.1111);
        climbingLocation.setLongitude(-1.1111);
        return climbingLocation;
    }

    /**
     * Creates a dummy {@link NewClimbingLocationRequest} object to be used in the tests
     *
     * @return the dummy {@link NewClimbingLocationRequest} object
     */
    public static NewClimbingLocationRequest createDummySaveLocationRequest() {
        NewClimbingLocationRequest request = new NewClimbingLocationRequest();
        request.setUserId(3);
        request.setPlaceId("place-id");
        request.setOutdoorLocation(true);
        return request;
    }

    /**
     * Creates a dummy {@link RemoveLocationRequest} object to be used in the tests
     *
     * @return the dummy {@link RemoveLocationRequest} object
     */
    public static RemoveLocationRequest createDummyRemoveLocationRequest() {
        RemoveLocationRequest request = new RemoveLocationRequest();
        request.setUserId(1);
        request.setLocationId(5);
        return request;
    }

    /**
     * Creates a dummy {@link PlaceDetails} object to be used in the tests
     *
     * @return the dummy {@link PlaceDetails} object
     */
    public static PlaceDetails createDummyPlaceDetails() {
        PlaceDetails placeDetails = new PlaceDetails();
        placeDetails.setPlaceId("place-id");
        placeDetails.setName("The Cuttings, Portland");
        placeDetails.setFormattedAddress("Isle of Portland, Dorset, UK");
        PlaceCoordinates coords = new PlaceCoordinates();
        coords.setLatitude(50.1111);
        coords.setLongitude(-1.1111);
        Geometry geometry = new Geometry();
        geometry.setCoordinates(coords);
        placeDetails.setGeometry(geometry);

        AddressComponent localityAddressComponent = new AddressComponent();
        localityAddressComponent.setLongName("Portland");
        localityAddressComponent.setTypes(Collections.singletonList("postal_town"));

        AddressComponent countryAddressComponent = new AddressComponent();
        countryAddressComponent.setLongName("United Kingdom");
        countryAddressComponent.setTypes(Collections.singletonList("country"));

        List<AddressComponent> addressComponents = new ArrayList<AddressComponent>();
        addressComponents.add(localityAddressComponent);
        addressComponents.add(countryAddressComponent);
        placeDetails.setAddressComponents(addressComponents);

        return placeDetails;
    }

    /**
     * Creates a dummy {@link NearbyPlace} object to be used in the tests
     *
     * @return the dummy {@link NearbyPlace} object
     */
    public static NearbyPlace createDummyNearbyPlace() {
        NearbyPlace nearbyPlace = new NearbyPlace();
        nearbyPlace.setPlaceId("place-id");
        nearbyPlace.setName("Indoor climbing centre");
        nearbyPlace.setFormattedAddress("Indoor climbing centre, Dorset, UK");
        PlaceCoordinates coords = new PlaceCoordinates();
        coords.setLatitude(50.1111);
        coords.setLongitude(-1.1111);
        Geometry geometry = new Geometry();
        geometry.setCoordinates(coords);
        nearbyPlace.setGeometry(geometry);
        nearbyPlace.setBusinessStatus(BusinessStatus.CLOSED_PERMANENTLY);
        nearbyPlace.setUserRatingAvg(4.52);
        nearbyPlace.setUserRatingsCount(234);
        return nearbyPlace;
    }

    /**
     * Creates a dummy {@link NearbySearchResult} object to be used in the tests
     *
     * @param nearbyPlaces the list of {@link NearbyPlace}s that should be set as the results returned
     * @param status       the response status that should be set on the result
     * @return the dummy {@link NearbySearchResult}
     */
    public static NearbySearchResult createDummyNearbySearchResult(List<NearbyPlace> nearbyPlaces, String status) {
        NearbySearchResult dummyNearbySearchResult = new NearbySearchResult();
        dummyNearbySearchResult.setStatus(status);
        dummyNearbySearchResult.setResults(nearbyPlaces);
        return dummyNearbySearchResult;
    }
}
