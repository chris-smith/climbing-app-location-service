package com.personal.projects.climbing.locationservice.data;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.personal.projects.climbing.locationservice.enums.BusinessStatus;

/**
 * Stores basic information about a place returned from Google Maps' (nearby) 'Place Search' API.
 *
 * @author Chris Smith
 */
public class NearbyPlace {
    private String placeId;
    private String name;
    private String formattedAddress;
    private Geometry geometry;
    private BusinessStatus businessStatus;
    private double userRatingAvg;
    private int userRatingsCount;

    /**
     * @return the unique identifier of this place in the Places API
     */
    @JsonGetter("placeId")
    public String getPlaceId() {
        return this.placeId;
    }

    /**
     * @param placeId the placeId to set
     */
    @JsonSetter("place_id")
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    /**
     * @return the name of this place
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the user-friendly, formatted address of this place
     */
    @JsonGetter("formattedAddress")
    public String getFormattedAddress() {
        return this.formattedAddress;
    }

    /**
     * @param formattedAddress the formattedAddress to set
     */
    @JsonSetter("vicinity")
    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return the geometry information relating to this place
     */
    public Geometry getGeometry() {
        return this.geometry;
    }

    /**
     * @param geometry the geometry to set
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    /**
     * @return the current business status of this place if applicable e.g. temporarily closed
     */
    @JsonGetter("businessStatus")
    public BusinessStatus getBusinessStatus() {
        return businessStatus;
    }

    /**
     * @param businessStatus the businessStatus to set
     */
    @JsonSetter("business_status")
    public void setBusinessStatus(BusinessStatus businessStatus) {
        this.businessStatus = businessStatus;
    }

    /**
     * @return the average user rating for this place
     */
    @JsonGetter("userRatingAvg")
    public double getUserRatingAvg() {
        return userRatingAvg;
    }

    /**
     * @param userRatingAvg the userRatingAvg to set
     */
    @JsonSetter("rating")
    public void setUserRatingAvg(double userRatingAvg) {
        this.userRatingAvg = userRatingAvg;
    }

    /**
     * @return the total number of users that have rated this place
     */
    @JsonGetter("userRatingsCount")
    public int getUserRatingsCount() {
        return userRatingsCount;
    }

    /**
     * @param userRatingsCount the userRatingsCount to set
     */
    @JsonSetter("user_ratings_total")
    public void setUserRatingsCount(int userRatingsCount) {
        this.userRatingsCount = userRatingsCount;
    }
}
