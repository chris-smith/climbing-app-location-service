DELIMITER //

USE `location`//

CREATE TABLE IF NOT EXISTS `location`.`climbing_location`
(
	`user_id` INT NOT NULL COMMENT 'Unique identifier for the user that saved this climbing location',
	`location_id` INT NOT NULL COMMENT 'Unique identifier for the location that this climbing location record corresponds to',
	`outdoor_location` BOOLEAN NOT NULL COMMENT 'A boolean indicating whether this is an outdoor climbing location or not',
	PRIMARY KEY (`user_id`, `location_id`),
	FOREIGN KEY (location_id) REFERENCES location (id)
) ENGINE=InnoDB COMMENT='Stores climbing locations that a user has saved/favourited' CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci//

COMMIT//

DELIMITER ;
