package com.personal.projects.climbing.locationservice.processor;

import com.personal.projects.climbing.locationservice.data.*;
import com.personal.projects.climbing.locationservice.datastore.LocationDataStore;
import com.personal.projects.climbing.locationservice.enums.LocationType;
import com.personal.projects.climbing.locationservice.exception.PlaceDetailsException;
import com.personal.projects.climbing.locationservice.exception.PlaceSearchException;
import com.personal.projects.climbing.locationservice.rest.GoogleMapsEndpoints;
import com.personal.projects.climbing.locationservice.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link LocationProcessor} class.
 *
 * @author Chris Smith
 */
public class LocationProcessorTest {
    @Mock
    private GoogleMapsEndpoints googleMapsEndpoints;

    @Mock
    private LocationDataStore dataStore;

    @InjectMocks
    private LocationProcessor processor;

    /**
     * Initialises the mocks and sets any necessary fields on the class under test
     */
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(this.processor, "maxNearbyPlaces", 10);
    }

    /**
     * Tests that {@link LocationProcessor#processLocationSearchRequest(String)} returns the expected {@link Place}
     * when {@link GoogleMapsEndpoints#searchForLocation(String)} returns a valid result for the location searched for
     *
     * @throws PlaceSearchException
     */
    @Test
    public void processLocationSearchRequest_matchingPlaceExists_returnsExpectedResult() throws PlaceSearchException {
        // Arrange
        String locationName = "Swanage";
        Place dummyPlace = TestUtils.createDummyPlace();
        PlaceSearchResult dummyPlaceSearchResult = TestUtils.createDummyPlaceSearchResult(
                Collections.singletonList(dummyPlace),
                "OK");
        when(this.googleMapsEndpoints.searchForLocation(locationName)).thenReturn(dummyPlaceSearchResult);

        // Act
        Place place = this.processor.processLocationSearchRequest(locationName);

        // Assert
        verify(this.googleMapsEndpoints, times(1)).searchForLocation(locationName);
        assertEquals(place.getPlaceId(), dummyPlace.getPlaceId());
        assertEquals(place.getName(), dummyPlace.getName());
        assertEquals(place.getFormattedAddress(), dummyPlace.getFormattedAddress());
        assertEquals(
                place.getGeometry().getCoordinates().getLatitude(),
                dummyPlace.getGeometry().getCoordinates().getLatitude());
        assertEquals(
                place.getGeometry().getCoordinates().getLongitude(),
                dummyPlace.getGeometry().getCoordinates().getLongitude());
    }

    /**
     * Tests that {@link LocationProcessor#processLocationSearchRequest(String)} returns a null when
     * {@link GoogleMapsEndpoints#searchForLocation(String)} returns no matching results for the location searched for
     *
     * @throws PlaceSearchException
     */
    @Test
    public void processLocationSearchRequest_noMatchingPlace_returnsNull() throws PlaceSearchException {
        // Arrange
        String locationName = "Swanage";
        PlaceSearchResult dummyPlaceSearchResult = TestUtils.createDummyPlaceSearchResult(
                new ArrayList<Place>(),
                "OK");
        when(this.googleMapsEndpoints.searchForLocation(locationName)).thenReturn(dummyPlaceSearchResult);

        // Act
        Place place = this.processor.processLocationSearchRequest(locationName);

        // Assert
        verify(this.googleMapsEndpoints, times(1)).searchForLocation(locationName);
        assertNull(place);
    }

    /**
     * Tests that {@link LocationProcessor#processLocationSearchRequest(String)} returns a null when
     * {@link GoogleMapsEndpoints#searchForLocation(String)} returns a status indicating Location Service made a bad
     * request to the API
     *
     * @throws PlaceSearchException
     */
    @Test
    public void processLocationSearchRequest_badRequest_returnsNull() throws PlaceSearchException {
        // Arrange
        String locationName = "Swanage";
        PlaceSearchResult dummyPlaceSearchResult = TestUtils.createDummyPlaceSearchResult(
                new ArrayList<Place>(),
                "Bad Request");
        when(this.googleMapsEndpoints.searchForLocation(locationName)).thenReturn(dummyPlaceSearchResult);

        // Act
        Place place = this.processor.processLocationSearchRequest(locationName);

        // Assert
        verify(this.googleMapsEndpoints, times(1)).searchForLocation(locationName);
        assertNull(place);
    }

    /**
     * Tests that {@link LocationProcessor#processSaveLocationRequest(int, String, boolean)} returns the expected
     * {@link ClimbingLocation} when {@link GoogleMapsEndpoints#getLocationDetail(String)} returns a valid
     * {@link PlaceDetailsResponse}
     *
     * @throws PlaceDetailsException
     */
    @Test
    public void processSaveLocationRequest_successfulApiRequest_returnsExpectedResult() throws PlaceDetailsException {
        // Arrange
        PlaceDetails dummyPlaceDetails = TestUtils.createDummyPlaceDetails();
        PlaceDetailsResponse dummyPlaceDetailsResponse = new PlaceDetailsResponse();
        dummyPlaceDetailsResponse.setStatus("OK");
        dummyPlaceDetailsResponse.setResult(dummyPlaceDetails);
        int userId = 100;
        String placeId = "place-id";
        boolean outdoorLocation = true;
        int newLocationId = 10;
        when(this.googleMapsEndpoints.getLocationDetail(placeId)).thenReturn(dummyPlaceDetailsResponse);
        when(this.dataStore.insertClimbingLocation(any(ClimbingLocation.class))).thenReturn(newLocationId);

        // Act
        ClimbingLocation climbingLocation = this.processor.processSaveLocationRequest(
                userId,
                placeId,
                outdoorLocation);

        // Assert
        verify(this.googleMapsEndpoints, times(1)).getLocationDetail(placeId);
        verify(this.dataStore, times(1)).insertClimbingLocation(any(ClimbingLocation.class));

        assertEquals(userId, climbingLocation.getUserId());
        assertEquals(newLocationId, climbingLocation.getLocationId());
        assertEquals(outdoorLocation, climbingLocation.isOutdoorLocation());
        assertEquals(dummyPlaceDetails.getName(), climbingLocation.getName());
        assertEquals(dummyPlaceDetails.getAddressComponents().get(0).getLongName(), climbingLocation.getLocality());
        assertEquals(dummyPlaceDetails.getAddressComponents().get(1).getLongName(), climbingLocation.getCountry());
        assertEquals(dummyPlaceDetails.getGeometry().getCoordinates().getLatitude(), climbingLocation.getLatitude());
        assertEquals(dummyPlaceDetails.getGeometry().getCoordinates().getLongitude(), climbingLocation.getLongitude());
    }

    /**
     * Tests that {@link LocationProcessor#processSaveLocationRequest(int, String, boolean)} returns a null when
     * {@link GoogleMapsEndpoints#getLocationDetail(String)} returns a status indicating Location Service made a bad
     * request to the API
     *
     * @throws PlaceDetailsException
     */
    @Test
    public void processSaveLocationRequest_badRequest_returnsNull() throws PlaceDetailsException {
        // Arrange
        PlaceDetailsResponse dummyPlaceDetailsResponse = new PlaceDetailsResponse();
        dummyPlaceDetailsResponse.setStatus("Bad Request");
        dummyPlaceDetailsResponse.setResult(new PlaceDetails());
        int userId = 100;
        String placeId = "place-id";
        boolean outdoorLocation = true;
        when(this.googleMapsEndpoints.getLocationDetail(placeId)).thenReturn(dummyPlaceDetailsResponse);

        // Act
        ClimbingLocation climbingLocation = this.processor.processSaveLocationRequest(userId, placeId, outdoorLocation);

        // Assert
        verify(this.googleMapsEndpoints, times(1)).getLocationDetail(placeId);
        verify(this.dataStore, never()).insertClimbingLocation(any(ClimbingLocation.class));
        assertNull(climbingLocation);
    }

    /**
     * Tests that {@link LocationProcessor#processGetClimbingLocationsRequest(int, boolean)} calls
     * {@link LocationDataStore#getOutdoorClimbingLocations(int)} the expected number of times when it is an outdoor
     * locations request
     */
    @Test
    public void processGetClimbingLocationsRequest_outdoorLocationsRequest_callsCorrectDatastoreMethod() {
        // Arrange
        int userId = 10;

        // Act
        this.processor.processGetClimbingLocationsRequest(userId, true);

        // Assert
        verify(this.dataStore, times(1)).getOutdoorClimbingLocations(userId);
        verify(this.dataStore, never()).getIndoorClimbingLocations(userId);
    }

    /**
     * Tests that {@link LocationProcessor#processGetClimbingLocationsRequest(int, boolean)} calls
     * {@link LocationDataStore#getIndoorClimbingLocations(int)} the expected number of times when it is an indoor
     * locations request
     */
    @Test
    public void processGetClimbingLocationsRequest_indoorLocationsRequest_callsCorrectDatastoreMethod() {
        // Arrange
        int userId = 10;

        // Act
        this.processor.processGetClimbingLocationsRequest(userId, false);

        // Assert
        verify(this.dataStore, times(1)).getIndoorClimbingLocations(userId);
        verify(this.dataStore, never()).getOutdoorClimbingLocations(userId);
    }

    /**
     * Tests that {@link LocationProcessor#processGetNearestLocationsRequest(String, String, LocationType)} returns the
     * expected list of {@link NearbyPlace}s when
     * {@link GoogleMapsEndpoints#findNearestLocations(String, String, String)} returns a valid {@link NearbySearchResult}
     *
     * @throws PlaceSearchException
     */
    @Test
    public void processGetNearestLocationsRequest_successfulApiRequest_returnsExpectedResult() throws PlaceSearchException {
        // Arrange
        NearbyPlace dummyNearbyPlace = TestUtils.createDummyNearbyPlace();
        NearbySearchResult nearbySearchResult = TestUtils.createDummyNearbySearchResult(
                Collections.singletonList(dummyNearbyPlace),
                "OK");

        String latitude = "5.1111";
        String longitude = "-1.1111";
        LocationType locationType = LocationType.OUTDOOR_SHOP;
        when(this.googleMapsEndpoints.findNearestLocations(latitude, longitude, locationType.toString()))
                .thenReturn(nearbySearchResult);

        // Act
        List<NearbyPlace> nearbyPlaces = this.processor.processGetNearestLocationsRequest(
                latitude,
                longitude,
                locationType);

        // Assert
        verify(this.googleMapsEndpoints, times(1))
                .findNearestLocations(latitude, longitude, locationType.toString());

        assertEquals(1, nearbyPlaces.size());
        assertEquals(dummyNearbyPlace.getPlaceId(), nearbyPlaces.get(0).getPlaceId());
        assertEquals(dummyNearbyPlace.getName(), nearbyPlaces.get(0).getName());
        assertEquals(dummyNearbyPlace.getFormattedAddress(), nearbyPlaces.get(0).getFormattedAddress());
        assertEquals(
                dummyNearbyPlace.getGeometry().getCoordinates().getLatitude(),
                nearbyPlaces.get(0).getGeometry().getCoordinates().getLatitude());
        assertEquals(
                dummyNearbyPlace.getGeometry().getCoordinates().getLongitude(),
                nearbyPlaces.get(0).getGeometry().getCoordinates().getLongitude());
        assertEquals(dummyNearbyPlace.getBusinessStatus(), nearbyPlaces.get(0).getBusinessStatus());
        assertEquals(dummyNearbyPlace.getUserRatingAvg(), nearbyPlaces.get(0).getUserRatingAvg());
        assertEquals(dummyNearbyPlace.getUserRatingsCount(), nearbyPlaces.get(0).getUserRatingsCount());
    }

    /**
     * Tests that {@link LocationProcessor#processGetNearestLocationsRequest(String, String, LocationType)} returns
     * an empty list when {@link GoogleMapsEndpoints#findNearestLocations(String, String, String)} returns a status
     * indicating Location Service made a bad request to the API
     *
     * @throws PlaceSearchException
     */
    @Test
    public void processGetNearestLocationsRequest_badRequest_returnsEmptyList() throws PlaceSearchException {
        // Arrange
        NearbySearchResult nearbySearchResult = TestUtils.createDummyNearbySearchResult(
                Collections.singletonList(new NearbyPlace()),
                "Bad Request");

        String latitude = "5.1111";
        String longitude = "-1.1111";
        LocationType locationType = LocationType.OUTDOOR_SHOP;
        when(this.googleMapsEndpoints.findNearestLocations(latitude, longitude, locationType.toString()))
                .thenReturn(nearbySearchResult);

        // Act
        List<NearbyPlace> nearbyPlaces = this.processor.processGetNearestLocationsRequest(
                latitude,
                longitude,
                locationType);

        // Assert
        assertEquals(0, nearbyPlaces.size());
    }
}
