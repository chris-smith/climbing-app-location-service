package com.personal.projects.climbing.locationservice.datastore.rowmapper;

import com.personal.projects.climbing.locationservice.data.ClimbingLocation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps records from the 'climbing_location' database table to {@link ClimbingLocation} objects.
 *
 * @author Chris Smith
 */
public class ClimbingLocationRowMapper implements RowMapper<ClimbingLocation> {
    @Override
    public ClimbingLocation mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        ClimbingLocation climbingLocation = new ClimbingLocation();
        climbingLocation.setUserId(resultSet.getInt("user_id"));
        climbingLocation.setLocationId(resultSet.getInt("id"));
        climbingLocation.setOutdoorLocation(resultSet.getBoolean("outdoor_location"));
        climbingLocation.setName(resultSet.getString("location_name"));
        climbingLocation.setLocality(resultSet.getString("locality"));
        climbingLocation.setCounty(resultSet.getString("county"));
        climbingLocation.setCountry(resultSet.getString("country"));
        climbingLocation.setPostCode(resultSet.getString("post_code"));
        climbingLocation.setLatitude(resultSet.getDouble("latitude"));
        climbingLocation.setLongitude(resultSet.getDouble("longitude"));
        return climbingLocation;
    }
}
