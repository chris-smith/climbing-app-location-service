package com.personal.projects.climbing.locationservice.data;

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.List;

/**
 * Stores information about a single address component of a place returned from Google Maps' 'Place Details' API. For
 * example, a post code component or country component.
 *
 * @author Chris Smith
 */
public class AddressComponent {
    private String longName;
    private String shortName;
    private List<String> types;

    /**
     * @return the long name given for this address component
     */
    public String getLongName() {
        return this.longName;
    }

    /**
     * @param longName the longName to set
     */
    @JsonSetter("long_name")
    public void setLongName(String longName) {
        this.longName = longName;
    }

    /**
     * @return the short name given for this address component
     */
    public String getShortName() {
        return this.shortName;
    }

    /**
     * @param shortName the shortName to set
     */
    @JsonSetter("short_name")
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /**
     * @return a list of strings representing what type of address component this is
     */
    public List<String> getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(List<String> types) {
        this.types = types;
    }
}
