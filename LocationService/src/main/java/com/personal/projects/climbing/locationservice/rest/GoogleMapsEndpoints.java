package com.personal.projects.climbing.locationservice.rest;

import com.personal.projects.climbing.locationservice.data.NearbySearchResult;
import com.personal.projects.climbing.locationservice.data.PlaceDetailsResponse;
import com.personal.projects.climbing.locationservice.data.PlaceSearchResult;
import com.personal.projects.climbing.locationservice.exception.PlaceDetailsException;
import com.personal.projects.climbing.locationservice.exception.PlaceSearchException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;

/**
 * Class used for making REST calls to Google Maps' APIs. This includes the Place Search and Place Details APIs - see
 * https://developers.google.com/maps/documentation/places/web-service/overview for more info.
 *
 * @author Chris Smith
 */
@Component
public class GoogleMapsEndpoints {
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleMapsEndpoints.class);

    @Value("${google.maps.api.place-search-url-template}")
    private String placeSearchUrlTemplate;

    @Value("${google.maps.api.place-details-url-template}")
    private String placeDetailsUrlTemplate;

    @Value("${google.maps.api.nearby-search-url-template}")
    private String nearbySearchUrlTemplate;

    @Autowired
    private WebClient webClient;

    /**
     * Retrieves the basic information about the location that is returned from the Place Search API for the specified
     * location name
     *
     * @param locationName the name of the location to search for
     * @return a {@link PlaceSearchResult} object storing the details retrieved from the API
     * @throws PlaceSearchException on failure to retrieve a valid response from the API
     */
    public PlaceSearchResult searchForLocation(String locationName) throws PlaceSearchException {
        String url = String.format(this.placeSearchUrlTemplate, locationName);
        LOGGER.debug("Making request to {} to retrieve the basic location details for {}", url, locationName);

        try {
            return this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(PlaceSearchResult.class)
                    .block();
        } catch (WebClientResponseException e) {
            throw new PlaceSearchException(e.getMessage(), e.getStatusCode(), e);
        }
    }

    /**
     * Retrieves detailed information about the location with the specified place ID, from the Place Details API. The
     * place ID is returned for a location from the Place Search API.
     *
     * @param placeId the place ID of the location to get details of, as returned from the Place Search API
     * @return a {@link PlaceDetailsResponse} object storing the details retrieved from the API
     * @throws PlaceDetailsException on failure to retrieve a valid response from the API
     */
    public PlaceDetailsResponse getLocationDetail(String placeId) throws PlaceDetailsException {
        String url = String.format(this.placeDetailsUrlTemplate, placeId);
        LOGGER.debug("Making request to {} to retrieve full location details for place with ID {}", url, placeId);

        try {
            return this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(PlaceDetailsResponse.class)
                    .block();
        } catch (WebClientResponseException e) {
            throw new PlaceDetailsException(e.getMessage(), e.getStatusCode(), e);
        }
    }

    /**
     * Retrieves basic information about the nearest places of the specified type to the specified coordinates,
     * via the (nearby) Place Search API
     *
     * @param latitude     the latitude of the location to find the nearest places of the specified type for
     * @param longitude    the longitude of the location to find the nearest places of the specified type for
     * @param locationType the type of location to search for e.g. climbing centres or outdoor shops
     * @return a {@link NearbySearchResult} object storing the details retrieved from the API
     * @throws PlaceSearchException on failure to retrieve a valid response from the API
     */
    public NearbySearchResult findNearestLocations(String latitude, String longitude, String locationType)
            throws PlaceSearchException {
        String url = String.format(this.nearbySearchUrlTemplate, latitude, longitude, locationType);
        LOGGER.debug("Making request to {} to find nearest {}s to location with latitude {}, longitude {}",
                url, locationType, latitude, longitude);

        try {
            return this.webClient.get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(NearbySearchResult.class)
                    .block();
        } catch (WebClientResponseException e) {
            throw new PlaceSearchException(e.getMessage(), e.getStatusCode(), e);
        }
    }
}
