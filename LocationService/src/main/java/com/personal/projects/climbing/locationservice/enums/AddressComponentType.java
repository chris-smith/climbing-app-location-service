package com.personal.projects.climbing.locationservice.enums;

/**
 * An enum that stores the different types of address component that can be returned for a location from Google Maps'
 * 'Place Details' API.
 *
 * @author Chris Smith
 */
public enum AddressComponentType {
    LOCALITY("postal_town"),
    COUNTY("administrative_area_level_2"),
    COUNTRY("country"),
    POST_CODE("postal_code");

    private final String value;

    /**
     * Constructor for creating an instance of {@link AddressComponentType}
     *
     * @param value the value to set
     */
    AddressComponentType(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
