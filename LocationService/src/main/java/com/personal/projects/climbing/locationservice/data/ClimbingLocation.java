package com.personal.projects.climbing.locationservice.data;

/**
 * Stores information about a climbing location. Represents a combination of data from the 'location' and
 * 'climbing_location' database tables.
 *
 * @author Chris Smith
 */
public class ClimbingLocation {
    private int userId;
    private int locationId;
    private boolean outdoorLocation;
    private String name;
    private String locality;
    private String county;
    private String country;
    private String postCode;
    private double latitude;
    private double longitude;

    /**
     * @return the unique identifier for a user that saved this location
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the unique identifier for the location
     */
    public int getLocationId() {
        return this.locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    /**
     * @return a boolean indicating whether this is an outdoor climbing location or not
     */
    public boolean isOutdoorLocation() {
        return this.outdoorLocation;
    }

    /**
     * @param outdoorLocation the outdoorLocation to set
     */
    public void setOutdoorLocation(boolean outdoorLocation) {
        this.outdoorLocation = outdoorLocation;
    }

    /**
     * @return the name of the location
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the town, village or city of the location
     */
    public String getLocality() {
        return this.locality;
    }

    /**
     * @param locality the locality to set
     */
    public void setLocality(String locality) {
        this.locality = locality;
    }

    /**
     * @return the county or state of the location
     */
    public String getCounty() {
        return this.county;
    }

    /**
     * @param county the county to set
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * @return the country of the location
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the post code of the location
     */
    public String getPostCode() {
        return this.postCode;
    }

    /**
     * @param postCode the postCode to set
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the latitude of the place
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude of the place
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
