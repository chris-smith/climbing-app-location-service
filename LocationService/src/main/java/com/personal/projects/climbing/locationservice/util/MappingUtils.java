package com.personal.projects.climbing.locationservice.util;

import com.personal.projects.climbing.locationservice.data.AddressComponent;
import com.personal.projects.climbing.locationservice.data.ClimbingLocation;
import com.personal.projects.climbing.locationservice.data.PlaceDetails;
import com.personal.projects.climbing.locationservice.enums.AddressComponentType;

import java.util.List;

/**
 * Utility class providing methods for mapping between external and internal objects.
 *
 * @author Chris Smith
 */
public final class MappingUtils {
    /**
     * Default private constructor for {@link MappingUtils} to prevent instantiation
     */
    private MappingUtils() {

    }

    /**
     * Maps a {@link PlaceDetails} object returned from Google Maps' 'Place Details' API to a {@link ClimbingLocation}
     * object that can be used within the Location Service
     *
     * @param userId          the unique identifier for the user that should be associated with the new climbing
     *                        location
     * @param outdoorLocation a boolean indicating whether the climbing location is outdoors or not
     * @param placeDetails    the {@link PlaceDetails} as returned from the Place Details API
     * @return the {@link ClimbingLocation}
     */
    public static ClimbingLocation placeDetailsToClimbingLocation(
            int userId,
            boolean outdoorLocation,
            PlaceDetails placeDetails) {
        List<AddressComponent> addressComponents = placeDetails.getAddressComponents();
        AddressComponent localityComponent = addressComponents.stream()
                .filter(c -> c.getTypes().contains(AddressComponentType.LOCALITY.toString()))
                .findFirst()
                .orElse(null);
        String locality = localityComponent == null ? null : localityComponent.getLongName();

        AddressComponent countyComponent = addressComponents.stream()
                .filter(c -> c.getTypes().contains(AddressComponentType.COUNTY.toString()))
                .findFirst()
                .orElse(null);
        String county = countyComponent == null ? null : countyComponent.getLongName();

        AddressComponent countryComponent = addressComponents.stream()
                .filter(c -> c.getTypes().contains(AddressComponentType.COUNTRY.toString()))
                .findFirst()
                .orElse(null);
        String country = countryComponent == null ? null : countryComponent.getLongName();

        AddressComponent postCodeComponent = addressComponents.stream()
                .filter(c -> c.getTypes().contains(AddressComponentType.POST_CODE.toString()))
                .findFirst()
                .orElse(null);
        String postCode = postCodeComponent == null ? null : postCodeComponent.getLongName();

        ClimbingLocation climbingLocation = new ClimbingLocation();
        climbingLocation.setUserId(userId);
        climbingLocation.setOutdoorLocation(outdoorLocation);
        climbingLocation.setName(placeDetails.getName());
        climbingLocation.setLocality(locality);
        climbingLocation.setCounty(county);
        climbingLocation.setCountry(country);
        climbingLocation.setPostCode(postCode);
        climbingLocation.setLatitude(placeDetails.getGeometry().getCoordinates().getLatitude());
        climbingLocation.setLongitude(placeDetails.getGeometry().getCoordinates().getLongitude());

        return climbingLocation;
    }
}
