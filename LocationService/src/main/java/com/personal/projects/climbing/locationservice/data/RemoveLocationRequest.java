package com.personal.projects.climbing.locationservice.data;

/**
 * Contains details of a location to be removed from a user's saved locations in the database.
 *
 * @author Chris Smith
 */
public class RemoveLocationRequest {
    private int userId;
    private int locationId;

    /**
     * @return the unique identifier of the user that has requested the removal of a location
     */
    public int getUserId() {
        return this.userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the unique identifier of the location that is to be removed from the user's saved locations
     */
    public int getLocationId() {
        return this.locationId;
    }

    /**
     * @param locationId the placeId to set
     */
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
}
