DELIMITER //

DROP PROCEDURE IF EXISTS `location`.`usp_climbing_location_delete`//

CREATE PROCEDURE `location`.`usp_climbing_location_delete`(
	IN user_id_in 		INT,
	IN location_id_in 	INT
)
BEGIN

	DELETE FROM	`location`.`climbing_location`
	WHERE user_id = user_id_in AND location_id = location_id_in;

END//

COMMIT//

DELIMITER ;
