package com.personal.projects.climbing.locationservice.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * An enum that stores the different types of business status that a place returned from Google Maps' (nearby) 'Place
 * Search' API can have, if applicable.
 *
 * @author Chris Smith
 */
public enum BusinessStatus {
    OPERATIONAL("Operational"),
    CLOSED_TEMPORARILY("Closed temporarily"),
    CLOSED_PERMANENTLY("Closed permanently");

    private final String value;

    /**
     * Constructor for creating an instance of {@link BusinessStatus}
     *
     * @param value the value to set
     */
    BusinessStatus(final String value) {
        this.value = value;
    }

    /**
     * Method used when deserialising from a JSON string value to a {@link BusinessStatus}
     *
     * @param statusToDeserialise the status in string form
     * @return the {@link BusinessStatus} if there is a match; otherwise null
     */
    @JsonCreator
    public static BusinessStatus deserialise(String statusToDeserialise) {
        for (BusinessStatus status : BusinessStatus.values()) {
            if (status.name().equals(statusToDeserialise)) {
                return status;
            }
        }

        return null;
    }

    @JsonValue
    @Override
    public String toString() {
        return this.value;
    }
}
