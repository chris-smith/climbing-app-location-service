package com.personal.projects.climbing.locationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for the Location Service application.
 *
 * @author Chris Smith
 */
@SpringBootApplication
public class LocationService {
    /**
     * Entry point method for the {@link LocationService}
     *
     * @param args string array of command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(LocationService.class, args);
    }
}
