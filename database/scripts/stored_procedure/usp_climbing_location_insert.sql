DELIMITER //

DROP PROCEDURE IF EXISTS `location`.`usp_climbing_location_insert`//

CREATE PROCEDURE `location`.`usp_climbing_location_insert`(
	IN user_id_in 			INT,
	IN location_name_in 	VARCHAR(100),
	IN locality_in 			VARCHAR(100),
	IN county_in 			VARCHAR(100),
	IN country_in 			VARCHAR(100),
	IN post_code_in 		VARCHAR(20),
	IN latitude_in 			DECIMAL(10, 8),
	IN longitude_in 		DECIMAL(11, 8),
	IN outdoor_location_in 	BOOLEAN,
	OUT location_id_out 	INT
)
BEGIN

	SET @locationId = (SELECT id FROM `location`.`location` WHERE latitude = latitude_in AND longitude = longitude_in LIMIT 1);
	IF @locationId IS NULL THEN
		INSERT INTO `location`.`location`(`location_name`, `locality`, `county`, `country`, `post_code`, `latitude`, `longitude`)
		VALUES (location_name_in, locality_in, county_in, country_in, post_code_in, latitude_in, longitude_in);
		SET @locationId = LAST_INSERT_ID();
	END IF;

	INSERT INTO `location`.`climbing_location`(`user_id`, `location_id`, `outdoor_location`)
	VALUES (user_id_in, @locationId, outdoor_location_in);
	
	SELECT @locationId INTO location_id_out;

END//

COMMIT//

DELIMITER ;
